#
# Licensed to the Apache Software Foundation (ASF) under one or more
# contributor license agreements.  See the NOTICE file distributed with
# this work for additional information regarding copyright ownership.
# The ASF licenses this file to You under the Apache License, Version 2.0
# (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Default system properties included when running spark-submit.
# This is useful for setting default environmental settings.

# Example:
# spark.master                     spark://master:7077
# spark.eventLog.enabled           true
# spark.eventLog.dir               hdfs://namenode:8021/directory
# spark.serializer                 org.apache.spark.serializer.KryoSerializer
# spark.driver.memory              5g
# spark.executor.extraJavaOptions  -XX:+PrintGCDetails -Dkey=value -Dnumbers="one two three"
#


#####################################################
# Spark defaults                                    #
#####################################################

spark.driver.extraJavaOptions   -Dservice.url=https://cs-ccr-nxcals6.cern.ch:19093,https://cs-ccr-nxcals7.cern.ch:19093,https://cs-ccr-nxcals8.cern.ch:19093 

spark.sql.caseSensitive true
spark.sql.execution.arrow.enabled true
spark.debug.maxToStringFields 50

spark.eventLog.enabled  true
spark.eventLog.dir  hdfs://nxcals//var/log/spark-history
spark.history.fs.logDirectory   hdfs://nxcals//var/log/spark-history

spark.history.fs.cleaner.enabled    true
spark.history.fs.cleaner.maxAge     60d

spark.hbase.connector.meta.cache.enabled true
spark.hbase.connector.meta.cache.expiry 10m

spark.driver.port                       5001
spark.blockManager.port                 5101
spark.ui.port                           5201
spark.port.maxRetries                   99




spark.jars nxcals-jars/activation-1.1.1.jar,nxcals-jars/aircompressor-0.10.jar,nxcals-jars/amqp-client-4.4.1.jar,nxcals-jars/animal-sniffer-annotation-1.0.jar,nxcals-jars/animal-sniffer-annotations-1.17.jar,nxcals-jars/annotations-2.0.0.jar,nxcals-jars/antlr-2.7.7.jar,nxcals-jars/antlr4-runtime-4.7.jar,nxcals-jars/antlr-runtime-3.4.jar,nxcals-jars/aopalliance-1.0.jar,nxcals-jars/aopalliance-repackaged-2.4.0-b31.jar,nxcals-jars/apacheds-i18n-2.0.0-M15.jar,nxcals-jars/apacheds-kerberos-codec-2.0.0-M15.jar,nxcals-jars/api-asn1-api-1.0.0-M20.jar,nxcals-jars/api-util-1.0.0-M20.jar,nxcals-jars/archaius-core-0.6.6.jar,nxcals-jars/arrow-format-0.10.0.jar,nxcals-jars/arrow-memory-0.10.0.jar,nxcals-jars/arrow-vector-0.10.0.jar,nxcals-jars/asm-3.1.jar,nxcals-jars/aspectjrt-1.8.10.jar,nxcals-jars/aspectjweaver-1.8.10.jar,nxcals-jars/avro-1.8.2.jar,nxcals-jars/avro-ipc-1.8.2.jar,nxcals-jars/avro-mapred-1.8.2-hadoop2.jar,nxcals-jars/bonecp-0.8.0.RELEASE.jar,nxcals-jars/caffeine-2.6.2.jar,nxcals-jars/calcite-avatica-1.2.0-incubating.jar,nxcals-jars/calcite-core-1.2.0-incubating.jar,nxcals-jars/calcite-linq4j-1.2.0-incubating.jar,nxcals-jars/cglib-2.2.1-v20090111.jar,nxcals-jars/checker-qual-2.5.2.jar,nxcals-jars/chill_2.11-0.9.3.jar,nxcals-jars/chill-java-0.9.3.jar,nxcals-jars/classmate-1.3.4.jar,nxcals-jars/cmw-data-2.0.3.jar,nxcals-jars/cmw-datax-1.0.0.jar,nxcals-jars/commons-cli-1.2.jar,nxcals-jars/commons-codec-1.11.jar,nxcals-jars/commons-collections-3.2.2.jar,nxcals-jars/commons-compiler-3.0.9.jar,nxcals-jars/commons-compress-1.8.1.jar,nxcals-jars/commons-configuration-1.10.jar,nxcals-jars/commons-crypto-1.0.0.jar,nxcals-jars/commons-dbcp-1.4.jar,nxcals-jars/commons-el-1.0.jar,nxcals-jars/commons-httpclient-3.1.jar,nxcals-jars/commons-io-2.6.jar,nxcals-jars/commons-lang-2.6.jar,nxcals-jars/commons-lang3-3.8.1.jar,nxcals-jars/commons-logging-1.2.jar,nxcals-jars/commons-math-2.2.jar,nxcals-jars/commons-math3-3.6.1.jar,nxcals-jars/commons-net-3.1.jar,nxcals-jars/commons-pool-1.5.4.jar,nxcals-jars/compress-lzf-1.0.3.jar,nxcals-jars/config-1.3.1.jar,nxcals-jars/curator-client-2.13.0.jar,nxcals-jars/curator-framework-2.13.0.jar,nxcals-jars/curator-recipes-2.13.0.jar,nxcals-jars/datanucleus-api-jdo-3.2.6.jar,nxcals-jars/datanucleus-core-3.2.10.jar,nxcals-jars/datanucleus-rdbms-3.2.9.jar,nxcals-jars/derby-10.12.1.1.jar,nxcals-jars/disruptor-3.3.0.jar,nxcals-jars/eigenbase-properties-1.1.5.jar,nxcals-jars/error_prone_annotations-2.2.0.jar,nxcals-jars/failureaccess-1.0.1.jar,nxcals-jars/feign-core-9.5.0.jar,nxcals-jars/feign-httpclient-9.5.0.jar,nxcals-jars/feign-jackson-9.5.0.jar,nxcals-jars/feign-ribbon-9.5.0.jar,nxcals-jars/feign-slf4j-9.5.0.jar,nxcals-jars/findbugs-annotations-1.3.9-1.jar,nxcals-jars/flatbuffers-1.2.0-3f79e055.jar,nxcals-jars/gson-2.8.5.jar,nxcals-jars/guava-27.0.1-jre.jar,nxcals-jars/guice-3.0.jar,nxcals-jars/guice-servlet-3.0.jar,nxcals-jars/hadoop-annotations-2.7.5.1.jar,nxcals-jars/hadoop-auth-2.7.5.1.jar,nxcals-jars/hadoop-client-2.7.5.1.jar,nxcals-jars/hadoop-common-2.7.5.1.jar,nxcals-jars/hadoop-hdfs-2.7.5.1.jar,nxcals-jars/hadoop-mapreduce-client-app-2.7.5.1.jar,nxcals-jars/hadoop-mapreduce-client-common-2.7.5.1.jar,nxcals-jars/hadoop-mapreduce-client-core-2.7.5.1.jar,nxcals-jars/hadoop-mapreduce-client-jobclient-2.7.5.1.jar,nxcals-jars/hadoop-mapreduce-client-shuffle-2.7.5.1.jar,nxcals-jars/hadoop-yarn-api-2.7.5.1.jar,nxcals-jars/hadoop-yarn-client-2.7.5.1.jar,nxcals-jars/hadoop-yarn-common-2.7.5.1.jar,nxcals-jars/hadoop-yarn-server-common-2.7.5.1.jar,nxcals-jars/hadoop-yarn-server-nodemanager-2.7.5.1.jar,nxcals-jars/hadoop-yarn-server-web-proxy-2.7.5.1.jar,nxcals-jars/hamcrest-core-1.3.jar,nxcals-jars/hbase-annotations-1.4.9.jar,nxcals-jars/hbase-client-1.4.9.jar,nxcals-jars/hbase-common-1.4.9.jar,nxcals-jars/hbase-common-1.4.9-tests.jar,nxcals-jars/hbase-hadoop2-compat-1.4.9.jar,nxcals-jars/hbase-hadoop-compat-1.4.9.jar,nxcals-jars/hbase-metrics-1.4.9.jar,nxcals-jars/hbase-metrics-api-1.4.9.jar,nxcals-jars/hbase-prefix-tree-1.4.9.jar,nxcals-jars/hbase-procedure-1.4.9.jar,nxcals-jars/hbase-protocol-1.4.9.jar,nxcals-jars/hbase-server-1.4.9.jar,nxcals-jars/hibernate-validator-6.0.15.Final.jar,nxcals-jars/hive-exec-1.2.1.spark2.jar,nxcals-jars/hive-metastore-1.2.1.spark2.jar,nxcals-jars/hk2-api-2.4.0-b31.jar,nxcals-jars/hk2-locator-2.4.0-b31.jar,nxcals-jars/hk2-utils-2.4.0-b31.jar,nxcals-jars/hppc-0.7.2.jar,nxcals-jars/htrace-core-3.1.0-incubating.jar,nxcals-jars/httpclient-4.5.3.jar,nxcals-jars/httpcore-4.4.6.jar,nxcals-jars/ivy-2.4.0.jar,nxcals-jars/j2objc-annotations-1.1.jar,nxcals-jars/jackson-annotations-2.9.8.jar,nxcals-jars/jackson-core-2.9.8.jar,nxcals-jars/jackson-core-asl-1.9.13.jar,nxcals-jars/jackson-databind-2.9.8.jar,nxcals-jars/jackson-dataformat-yaml-2.9.8.jar,nxcals-jars/jackson-datatype-jdk8-2.9.8.jar,nxcals-jars/jackson-datatype-jsr310-2.9.8.jar,nxcals-jars/jackson-jaxrs-1.9.13.jar,nxcals-jars/jackson-mapper-asl-1.9.13.jar,nxcals-jars/jackson-module-parameter-names-2.9.8.jar,nxcals-jars/jackson-module-paranamer-2.9.8.jar,nxcals-jars/jackson-module-scala_2.11-2.9.8.jar,nxcals-jars/jackson-xc-1.9.13.jar,nxcals-jars/jamon-runtime-2.4.1.jar,nxcals-jars/janino-3.0.9.jar,nxcals-jars/jasper-compiler-5.5.23.jar,nxcals-jars/jasper-runtime-5.5.23.jar,nxcals-jars/JavaEWAH-0.3.2.jar,nxcals-jars/javassist-3.18.1-GA.jar,nxcals-jars/javax.annotation-api-1.2.jar,nxcals-jars/javax.inject-1.jar,nxcals-jars/javax.inject-2.4.0-b31.jar,nxcals-jars/java-xmlbuilder-0.4.jar,nxcals-jars/javax.servlet-api-4.0.1.jar,nxcals-jars/javax.ws.rs-api-2.0.1.jar,nxcals-jars/javolution-5.5.1.jar,nxcals-jars/jaxb-api-2.2.2.jar,nxcals-jars/jaxb-impl-2.2.3-1.jar,nxcals-jars/jboss-logging-3.3.2.Final.jar,nxcals-jars/jcl-over-slf4j-1.7.16.jar,nxcals-jars/jcodings-1.0.8.jar,nxcals-jars/jdo-api-3.0.1.jar,nxcals-jars/jersey-client-1.9.jar,nxcals-jars/jersey-client-2.22.jar,nxcals-jars/jersey-common-2.22.jar,nxcals-jars/jersey-container-servlet-2.22.jar,nxcals-jars/jersey-container-servlet-core-2.22.jar,nxcals-jars/jersey-core-1.9.jar,nxcals-jars/jersey-guava-2.22.jar,nxcals-jars/jersey-guice-1.9.jar,nxcals-jars/jersey-json-1.9.jar,nxcals-jars/jersey-media-jaxb-2.22.jar,nxcals-jars/jersey-server-1.9.jar,nxcals-jars/jersey-server-2.22.jar,nxcals-jars/jets3t-0.9.0.jar,nxcals-jars/jettison-1.1.jar,nxcals-jars/jetty-6.1.26.jar,nxcals-jars/jetty-sslengine-6.1.26.jar,nxcals-jars/jetty-util-6.1.26.jar,nxcals-jars/jline-0.9.94.jar,nxcals-jars/joda-time-2.9.9.jar,nxcals-jars/jodd-core-3.5.2.jar,nxcals-jars/joni-2.1.2.jar,nxcals-jars/jsch-0.1.54.jar,nxcals-jars/json4s-ast_2.11-3.5.3.jar,nxcals-jars/json4s-core_2.11-3.5.3.jar,nxcals-jars/json4s-jackson_2.11-3.5.3.jar,nxcals-jars/json4s-native_2.11-3.5.3.jar,nxcals-jars/json4s-scalap_2.11-3.5.3.jar,nxcals-jars/jsp-api-2.1.jar,nxcals-jars/jsr305-3.0.2.jar,nxcals-jars/jta-1.1.jar,nxcals-jars/jul-to-slf4j-1.7.16.jar,nxcals-jars/junit-4.12.jar,nxcals-jars/kryo-shaded-4.0.2.jar,nxcals-jars/leveldbjni-all-1.8.jar,nxcals-jars/libfb303-0.9.3.jar,nxcals-jars/libthrift-0.9.3.jar,nxcals-jars/listenablefuture-9999.0-empty-to-avoid-conflict-with-guava.jar,nxcals-jars/lz4-java-1.4.0.jar,nxcals-jars/metrics-core-2.2.0.jar,nxcals-jars/metrics-core-4.0.5.jar,nxcals-jars/metrics-graphite-4.0.5.jar,nxcals-jars/metrics-json-4.0.5.jar,nxcals-jars/metrics-jvm-4.0.5.jar,nxcals-jars/minlog-1.3.0.jar,nxcals-jars/netflix-commons-util-0.1.1.jar,nxcals-jars/netflix-statistics-0.1.1.jar,nxcals-jars/netty-3.9.9.Final.jar,nxcals-jars/netty-all-4.1.30.Final.jar,nxcals-jars/nxcals-common-0.1.164.jar,nxcals-jars/nxcals-data-access-0.1.164.jar,nxcals-jars/nxcals-extraction-starter-0.1.164.jar,nxcals-jars/nxcals-service-client-0.1.164.jar,nxcals-jars/objenesis-2.5.1.jar,nxcals-jars/opencsv-2.3.jar,nxcals-jars/orc-core-1.5.2-nohive.jar,nxcals-jars/orc-mapreduce-1.5.2-nohive.jar,nxcals-jars/orc-shims-1.5.2.jar,nxcals-jars/oro-2.0.8.jar,nxcals-jars/osgi-resource-locator-1.0.1.jar,nxcals-jars/paranamer-2.8.jar,nxcals-jars/parquet-column-1.10.0.jar,nxcals-jars/parquet-common-1.10.0.jar,nxcals-jars/parquet-encoding-1.10.0.jar,nxcals-jars/parquet-format-2.4.0.jar,nxcals-jars/parquet-hadoop-1.10.0.jar,nxcals-jars/parquet-hadoop-bundle-1.6.0.jar,nxcals-jars/parquet-jackson-1.10.0.jar,nxcals-jars/protobuf-java-2.5.0.jar,nxcals-jars/py4j-0.10.7.jar,nxcals-jars/pyrolite-4.13.jar,nxcals-jars/ribbon-core-2.1.1.jar,nxcals-jars/ribbon-loadbalancer-2.1.1.jar,nxcals-jars/RoaringBitmap-0.5.11.jar,nxcals-jars/rxjava-1.0.9.jar,nxcals-jars/scaffeine_2.11-2.5.0.jar,nxcals-jars/scala-java8-compat_2.11-0.8.0.jar,nxcals-jars/scala-library-2.11.12.jar,nxcals-jars/scala-parser-combinators_2.11-1.1.0.jar,nxcals-jars/scala-reflect-2.11.12.jar,nxcals-jars/scala-xml_2.11-1.0.6.jar,nxcals-jars/servlet-api-2.5-20081211.jar,nxcals-jars/servlet-api-2.5.jar,nxcals-jars/servo-core-0.9.2.jar,nxcals-jars/servo-internal-0.9.2.jar,nxcals-jars/shc-core-1.1.1-2.1-s_2.11-NXCALS.jar,nxcals-jars/slf4j-api-1.7.22.jar,nxcals-jars/snakeyaml-1.23.jar,nxcals-jars/snappy-0.2.jar,nxcals-jars/snappy-java-1.1.7.2.jar,nxcals-jars/ST4-4.0.4.jar,nxcals-jars/stax-api-1.0.1.jar,nxcals-jars/stax-api-1.0-2.jar,nxcals-jars/stream-2.7.0.jar,nxcals-jars/stringtemplate-3.2.1.jar,nxcals-jars/univocity-parsers-2.7.3.jar,nxcals-jars/unused-1.0.0.jar,nxcals-jars/validation-api-2.0.1.Final.jar,nxcals-jars/xbean-asm6-shaded-4.8.jar,nxcals-jars/xercesImpl-2.9.1.jar,nxcals-jars/xmlenc-0.52.jar,nxcals-jars/xz-1.5.jar,nxcals-jars/zookeeper-3.4.10.jar,nxcals-jars/zstd-jni-1.3.2-2.jar


#####################################################
# Spark YARN mode properties                        #
#####################################################

spark.yarn.appMasterEnv.JAVA_HOME   /var/nxcals/jdk1.8.0_121
spark.executorEnv.JAVA_HOME /var/nxcals/jdk1.8.0_121

spark.executorEnv.PYTHONPATH /cvmfs/sft.cern.ch/lcg/releases/Python/3.6.5-56635/x86_64-centos7-gcc7-opt/lib:/cvmfs/sft.cern.ch/lcg/releases/Python/3.6.5-56635/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages:/cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages

spark.executorEnv.LD_LIBRARY_PATH /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages:/cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages/tensorflow:/cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages/tensorflow/contrib/tensor_forest:/cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/lib/python3.6/site-packages/tensorflow/python/framework:/cvmfs/sft.cern.ch/lcg/releases/java/8u91-ae32f/x86_64-centos7-gcc7-opt/jre/lib/amd64:/cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/lib64:/cvmfs/sft.cern.ch/lcg/views/LCG_95apython3_nxcals/x86_64-centos7-gcc7-opt/lib:/cvmfs/sft.cern.ch/lcg/releases/gcc/7.3.0-cb1ee/x86_64-centos7/lib:/cvmfs/sft.cern.ch/lcg/releases/gcc/7.3.0-cb1ee/x86_64-centos7/lib64:/cvmfs/sft.cern.ch/lcg/releases/binutils/2.28-a983d/x86_64-centos7/lib:/usr/local/lib/

spark.yarn.jars hdfs:////project/nxcals/lib/spark-2.4.0/*.jar

spark.yarn.am.extraLibraryPath    /usr/lib/hadoop/lib/native
spark.executor.extraLibraryPath   /usr/lib/hadoop/lib/native

spark.yarn.historyServer.address        ithdp1001.cern.ch:18080
spark.yarn.access.hadoopFileSystems nxcals


# You can uncomment the following spark properties to directly
# configure spark deploy mode and master type.
#
# NOTE: this is the configuration point of view, equivalent to following spark command:
# $ /path/to/spark/bin/spark-shell --master yarn
#
# spark.master

# You can uncomment the following kerberos properties to directly
# configure spark against a kerberos keytab/principal entry

# spark.yarn.keytab   <file_location>
# spark.yarn.principal <login@CERN.CH>
