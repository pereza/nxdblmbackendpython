#!/bin/sh

echo "*** Preparing python virtual environment for NXCALS ***"

nxcals_python_env_name=nxcals-python3-env

echo "Check if Python3 is present on the system..."
python3_executable_path=`command -v python3`
if [ -z "$python3_executable_path" ]; then
        echo "ERROR: Python3 is not present, supported version 3.6.x"
        return 1
fi

version=$(python3 -V 2>&1)
parsedVersion=$(echo "${version//[^0-9]/}")

if [[  "$parsedVersion" -gt "360" && "$parsedVersion" -lt "370" ]]
then
    echo "Valid Python3 version $version"
else
    echo "WARN: Not supported Python3 version: $version, supported version is 3.6.x"
fi

pip_installed=`command -v pip`

if [ ! -z "$pip_installed" ]; then
 echo "Saving current pip packages to ref-packages.txt"
 pip freeze > ref-packages.txt
else
 echo "pip not installed, cannot re-install current set of packages to the new virtual env..."
fi

python3 -m venv $nxcals_python_env_name
if [ $? -eq 0 ]; then
  echo "...python virtual environment created!"
else
  echo "ERROR: Cannot create virtual environment!"
  return 2
fi

echo "Loading python virtual environment..."
source $nxcals_python_env_name/bin/activate
if [ $? -eq 0 ]; then
  echo "...done!"
else
  echo "ERROR: Cannot load Python3 virtual environment"
  return 3
fi

echo "Installing nxcals sources..."
pip -v install ./extra-packages/nxcals_data_access_python3-0.1.164-py3-none-any.whl --no-index --find-links ./extra-packages
if [ $? -eq 0 ]; then
  echo "...done!"
else
  echo "ERROR: Cannot install nxcals sources!"
  return 4
fi

read -p "Do you want to install packages from source Python3 environment (requires internet connection or proxy)? [Y/n]:" install_libs
if [ "$install_libs" == "Y" ]; then
  pip install -r ref-packages.txt
else
  echo "Not installing packages from previous Python3 env. You can always do it by executing 'pip install -r ref-packages.txt'"
fi

read -p "Do you want to install pandas and pyarrow for pandas_udf (requires internet connection or proxy)? [Y/n]:" install_pandas
if [ "$install_pandas" == "Y" ]; then
  pip install pyarrow pandas
else
  echo "Not installing pandas and pyarrow. You can always do it by executing 'pip install pyarrow pandas'"
fi


echo "*** Hints: ***"
echo "*** Use pySpark: '$ ./bin/pySpark <options>'"
echo "*** Use '$ deactivate' command to exit virtual env ***"
echo "*** Use '$ source $nxcals_python_env_name/bin/activate' command to activate virtual env again ***"
