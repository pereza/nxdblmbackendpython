import java.time.format._
import java.time._
import cern.nxcals.data.access.builders._
import cern.nxcals.common.utils.TimeUtils

println("Working on 1 day of data:")

val start = LocalDateTime.parse("2018-05-01T00:00:00", DateTimeFormatter.ISO_DATE_TIME).toInstant(ZoneOffset.UTC)
val end = LocalDateTime.parse("2018-05-02T00:00:00", DateTimeFormatter.ISO_DATE_TIME).toInstant(ZoneOffset.UTC)

val tgmData = DevicePropertyQuery.builder(spark).system("CMW").startTime(start).endTime(end).fields().entity().parameter("CPS.TGM/FULL-TELEGRAM.STRC").buildDataset()
val data = DevicePropertyQuery.builder(spark).system("CMW").startTime(start).endTime(end).fields().entity().parameter("FTN.QFO415S/Acquisition").buildDataset()

println("Calculating some basic information about current:")
data.describe("current").show

println("Showing current sum for all *EAST* users:")

data.where("selector like '%EAST%'").agg(sum('current)).show

println("Showing current sum for destination *TOF* using join:")

val tgmFiltered = tgmData.where("DEST like '%TOF%'")

tgmFiltered.count

tgmFiltered.join(data, "cyclestamp").agg(sum('current)).show

println("Showing access to nested types, here array elements")
tgmData.select("SPCON.elements").as[Array[String]].show

println("Counting cycles")

tgmData.groupBy("USER").count().show

println("Showing max and min dates for TGM data")
tgmData.agg(min('cyclestamp), max('cyclestamp)).selectExpr("`min(cyclestamp)` as min","`max(cyclestamp)` as max").withColumn("mindate",from_unixtime(expr("min/1000000000"))).withColumn("maxdate", from_unixtime(expr("max/1000000000"))).show
