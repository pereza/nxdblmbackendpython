import time

import requests

from property_pull import property_pull
import pytimber as pytimber
import datetime as dt

# from multiprocessing import Process

# mysqlDB variables



API_HOST = 'http://test-nxdblm-api2.web.cern.ch/'


def start_pull(_fill_infos, _devices=None):
    if _devices is None:
        _devices = ['BLMED.06L7.B1B30_TCPA.A6L7.B1']

    print(_fill_infos)
    if _fill_infos["endTime"] is None:
        return -1

    for iDevice in _devices:

        property_pull(_fill_infos['fillNumber'], 'ALL', iDevice,
                      _fill_infos['startTime'].strftime("%Y-%m-%d %H:%M:%S.000"),
                      _fill_infos['endTime'].strftime("%Y-%m-%d %H:%M:%S.000"),
                      _files_write=False)

        for iMode in _fill_infos["beamModes"]:
            print(iMode['mode'])
            property_pull(_fill_infos['fillNumber'], iMode['mode'], iDevice,
                          iMode['startTime'].strftime("%Y-%m-%d %H:%M:%S.000"),
                          iMode['endTime'].strftime("%Y-%m-%d %H:%M:%S.000"))

            '''p = Process(target=cern_nxcals_dblm_property_pull.cern_nxcals_dblm_property_pull,
                        args=(fill_infos['fillNumber'], iMode['mode'], 'BLMED.06L7.B1B30_TCPA.A6L7.B1',
                              iMode['startTime'].strftime("%y-%m-%d %H:%M:%S.000"),
                              iMode['endTime'].strftime("%y-%m-%d %H:%M:%S.000")))
            p.start()
            p.join()
            '''


def routine_pull(_start_fill=None, _end_fill=None):
    ldb = pytimber.LoggingDB()

    if _start_fill is None and _end_fill is None:
        res = ldb.get('HX:FILLN', dt.datetime.now())
        new_fill_number = int(res['HX:FILLN'][1][0])
        print(new_fill_number)

        # gcursor.execute('select max(name) from Fill limit 1;')
        # last_fill_pulled = gcursor.fetchone()[0]
        response = requests.get(API_HOST + 'fill/last')
        last_fill_pulled = response.json()['name']
        print(last_fill_pulled)

        if new_fill_number == last_fill_pulled:
            print('already pulled last fill')
            return -1
    else:
        new_fill_number = _end_fill
        last_fill_pulled = _start_fill

    response = requests.get(API_HOST + 'devices')
    devices = []
    for device in response.json()['devices']:
        devices.append(device['name'])

    # fill_number = 7444
    # fill_infos = ldb.getLHCFillData(fill_number, False)
    # start_pull(fill_infos, devices)

    for iFill in range(int(last_fill_pulled), int(new_fill_number)):
        fill_infos = ldb.getLHCFillData(str(iFill), False)

        # check if the fill is finished
        if fill_infos['endTime'] is None:
            break

        # check if data is too new
        midnight_stamp = dt.datetime.now().replace(second=0, minute=0, hour=0) - dt.timedelta(days=1)
        if midnight_stamp < fill_infos['endTime']:
            continue

        start_pull(fill_infos, devices)


if __name__ == '__main__':
    routine_pull(_start_fill=7440, _end_fill=7494)
